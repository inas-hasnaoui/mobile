import 'package:flutter/material.dart';
import 'package:tutorial_app/views/login.dart';
import 'package:tutorial_app/views/dashboard.dart';
import 'package:tutorial_app/views/categories.dart';
import 'package:tutorial_app/views/statistics.dart';
import 'package:tutorial_app/views/graphMachine.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './bottom_navy_bar.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home>{

  @override
  void initState(){
  
    super.initState();
  }
  
    @override
    Widget build(BuildContext context) {
        return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      home: MyHomePage(title: 'Digital Mobil App'),
    // routes: <String, WidgetBuilder>{
    //   '/home': (BuildContext context) => MyHomePage(title: 'Digital Mobil App'),
    // },//MyHomePage(title: 'Digital Mobil App'),
    );
  }

  
  
}
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  void _onItemTapped(int index) {
    setState(() {
      if(index == 4){
        logout();
      }
      _selectedIndex = index;
    });
  }
   List<Widget> _body = <Widget>[
      Dashboard(),
      Categories(),
      LineChart(),
      Dashboard()
    
    ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SafeArea(
        child: _body[_selectedIndex],
      ),

      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _selectedIndex,
        showElevation: true,
        itemCornerRadius: 8,
        curve: Curves.easeInBack,
        items: [
          BottomNavyBarItem(
            icon: Icon(Icons.apps),
            title: Text('Home'),
            activeColor: Colors.red,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            icon: Icon(Icons.business),
            title: Text('Categories'),
            activeColor: Colors.purpleAccent,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            icon: Icon(Icons.insert_chart ),
            title: Text(
              'Statistics',
            ),
            activeColor: Colors.pink,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            icon: Icon(Icons.settings),
            title: Text('Settings'),
            activeColor: Colors.blue,
            textAlign: TextAlign.center,
          ),
             BottomNavyBarItem(
            icon: Icon(Icons.exit_to_app),
            title: Text('Logout'),
            activeColor: Colors.blue,
            textAlign: TextAlign.center,
                     
          ),
        ],
        onItemSelected: _onItemTapped,
      ),
    );
  }
  void logout() async{
     SharedPreferences localStorage = await SharedPreferences.getInstance();
     var token = localStorage.getString('token');   
      localStorage.remove(token);
      //runApp(MaterialApp(home: Login()));
      runApp(MaterialApp(home: Login()));
    
  }
}
