import 'package:flutter/material.dart';
import 'package:tutorial_app/views/machines.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class Categorie {
  final String name;
  Categorie(this.name);
}

class Categories extends StatefulWidget{
  @override
  _CategoriesState createState() {
    return _CategoriesState();
  }
}

class _CategoriesState extends State<Categories>{

 var id = ["Soudage", "Assemblage", "Remplissage", "title 4", "title 5",];
 int _count = 0 ;

  @override
  void initState() {
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      
      body: ListView.builder(
        itemCount: id.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(id[index]),
            onTap: ()  {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Machines(categorie: id[index]),
                ),
              );
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () =>
          Alert(
        context: context,
        title: "Add",
        content: Column(
          children: <Widget>[
            TextField(
              decoration: InputDecoration(
                icon: Icon(Icons.account_circle),
                labelText: 'Categorie Name',
              ),
            ),
          ],
        ),
        buttons: [
          DialogButton(
            onPressed: () => Navigator.pop(context),
            child: Text(
              "ADD",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            
          )
        ]).show(),
         
        child: const Icon(Icons.add),
      ),
    );
  
  }

}

